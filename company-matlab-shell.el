;;; company-matlab-shell.el --- a matlab-shell-mode completion back-end for company-mode
;;
;; Copyright (C) 2016 Nate Chodosh
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 2
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(condition-case nil
    (require 'company)
  (error nil))
(eval-when-compile (require 'cl))
(require 'matlab)



(defun company-matlab-shell-tab (str)
     "Send [TAB] to the currently running matlab process and retrieve completion
of given str."
     (while (string-match "[^']\\('\\)\\($\\|[^']\\)" str)
       (setq str (replace-match "''" t t str 1)))
     (let ((completions (matlab-shell-completion-list str)))
       completions))

;; (defun company-matlab-shell-tab (str)
;;    "Send [TAB] to the currently running matlab process and retrieve completion
;; of given str."
;;    (let ((orig-point (point)))
;;     (goto-char (point-max))
;;     (let ((inhibit-field-text-motion t))
;;       (beginning-of-line))
;;     (re-search-forward comint-prompt-regexp)
;;     (let* ((lastcmd (buffer-substring (point) (matlab-point-at-eol)))
;;            (tempcmd lastcmd)
;;            (completions nil)
;;            (limitpos nil))
;;       ;; search for character which limits completion, and limit command to it
;;       ;; (setq limitpos
;;       ;;       (if (string-match ".*\\([( /[,;=']\\)" lastcmd)
;;       ;;           (1+ (match-beginning 1))
;;       ;;         0))
;;       ;; 
;;       ;; (setq lastcmd (substring lastcmd limitpos))
;;       ;; Whack the old command so we can insert it back later.
;;       (delete-region (+ (point) 0) (matlab-point-at-eol))
;;       ;; double every single quote
;;       (while (string-match "[^']\\('\\)\\($\\|[^']\\)" tempcmd)
;;         (setq tempcmd (replace-match "''" t t tempcmd 1)))
;;       ;; collect the list
;;       (setq completions (matlab-shell-completion-list tempcmd))
;;       (goto-char (point-max))
;;       (insert lastcmd)
;;       (goto-char orig-point)
;;       completions)))

(defun company-matlab-shell-grab-symbol ()
  (when (string= (buffer-name (current-buffer)) (concat "*" matlab-shell-buffer-name "*"))
    (save-excursion
      (let ((orig-point (point)))
       (goto-char (point-max))
       (let ((inhibit-field-text-motion t))
         (beginning-of-line))
       (re-search-forward comint-prompt-regexp)
       (let* ((lastcmd (buffer-substring (point) orig-point))
              limitpos)
         (setq limitpos
               (if (string-match ".*\\([( /[,;=']\\)" lastcmd)
                   (1+ (match-beginning 1))
                 0))
         (substring-no-properties lastcmd limitpos))))))


(defun company-matlab-shell-get-completions (str)
  (when (string= (buffer-name (current-buffer)) (concat "*" matlab-shell-buffer-name "*"))
    (company-matlab-shell-tab str)))

;;;###autoload
(defun company-matlab-shell (command &optional arg &rest ignored)
  "A `company-mode' completion back-end for Matlab-Shell."
  (interactive (list 'interactive))
  (case command
        ('interactive (company-begin-backend 'company-matlab-shell))
        ('prefix (company-matlab-shell-grab-symbol))
        ('candidates (company-matlab-shell-tab arg))
	('sorted t)))

(provide 'company-matlab-shell)
;;; company-matlab-shell.el ends here
